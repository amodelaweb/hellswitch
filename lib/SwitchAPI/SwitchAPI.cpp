#include "SwitchAPI.h"

/* API SERVER FUNCTIONS */
void SwitchAPIServices::getModuleInformationService(AsyncWebServerRequest *request, String &module_name)
{
  request->send_P(200, "application/json", myDataTemplate, [&module_name](const String &var) {
    return SwitchAPIHandlers::moduleTemplateProcessor(var, module_name);
  });
}

void SwitchAPIServices::getLightStateService(AsyncWebServerRequest *request, LightsMap &lightTriggers)
{
  String lightId = request->hasParam(LIGHT_ID) ? request->getParam(LIGHT_ID)->value() : "N/A";
  if (!lightTriggers.contains(lightId.c_str()))
  {
    return request->send_P(400, "application/json", genericResponse, [](const String &var) {
      return SwitchAPIHandlers::genericMessageTemplateProcessor(var, String("Not found light with this id"));
    });
  }
  uint8_t state = SwitchCore::getLightState(lightId.c_str(), lightTriggers);
  request->send_P(200, "application/json", lightStateTemplate, [lightId, state](const String &var) {
    return SwitchAPIHandlers::lightStateTemplateProcessor(var, lightId, state);
  });
}

void SwitchAPIServices::toggleLightStateService(AsyncWebServerRequest *request, LightsMap &lightTriggers)
{
  String lightId = request->hasParam(LIGHT_ID) ? request->getParam(LIGHT_ID)->value() : "N/A";
  if (!lightTriggers.contains(lightId.c_str()))
  {
    return request->send_P(400, "application/json", genericResponse, [](const String &var) {
      return SwitchAPIHandlers::genericMessageTemplateProcessor(var, String("Not found light with this id"));
    });
  }
  SwitchCore::toggleLight(lightId.c_str(), lightTriggers);
  String state = SwitchCore::getLightState(lightId.c_str(), lightTriggers) ? "ON" : "OFF";
  return request->send_P(200, "application/json", genericResponse, [lightId, state](const String &var) {
    return SwitchAPIHandlers::genericMessageTemplateProcessor(var, "Light " + lightId + " was turned " + state);
  });
}

void SwitchAPIServices::setLightStateService(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total, LightsMap &lightTriggers)
{
  String lightId = request->hasParam(LIGHT_ID) ? request->getParam(LIGHT_ID)->value() : "N/A";
  if (!lightTriggers.contains(lightId.c_str()))
  {
    return request->send_P(400, "application/json", genericResponse, [](const String &var) {
      return SwitchAPIHandlers::genericMessageTemplateProcessor(var, String("Not found light with this id"));
    });
  }
  DynamicJsonDocument doc(1024);
  DeserializationError error = deserializeJson(doc, (const char *)data);
  if (!error && doc.containsKey(STATE_P) && (doc[STATE_P] == 0 || doc[STATE_P] == 1))
  {
    uint8_t state = doc[STATE_P];
    SwitchCore::setLightState(lightId.c_str(), state, lightTriggers);
    String state_str = state ? "ON" : "OFF";
    return request->send_P(200, "application/json", genericResponse, [lightId, state_str](const String &var) {
      return SwitchAPIHandlers::genericMessageTemplateProcessor(var, "Light " + lightId + " was turned " + state_str);
    });
  }

  return request->send_P(400, "application/json", genericResponse, [](const String &var) {
    return SwitchAPIHandlers::genericMessageTemplateProcessor(var, String("Bad formatted request, please read the doc, dumb."));
  });
}

void SwitchAPIServices::setSwitchStateService(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total, LightsMap &lightTriggers, LightsMap &lightSwitches, NamingMap &switchMapping)
{
  String switchId = request->hasParam(SWITCH_ID_P) ? request->getParam(SWITCH_ID_P)->value() : "N/A";
  if (!lightSwitches.contains(switchId.c_str()))
  {
    return request->send_P(400, "application/json", genericResponse, [](const String &var) {
      return SwitchAPIHandlers::genericMessageTemplateProcessor(var, String("Not found switch with this id"));
    });
  }
  DynamicJsonDocument doc(1024);
  DeserializationError error = deserializeJson(doc, (const char *)data);
  if (!error && doc.containsKey(STATE_SP) && (doc[STATE_SP] == 0 || doc[STATE_SP] == 1))
  {
    uint8_t state = doc[STATE_SP];
    SwitchCore::setSwitchState(switchId.c_str(), state, lightTriggers, lightSwitches, switchMapping);
    String state_str = state ? "ON" : "OFF";
    return request->send_P(200, "application/json", genericResponse, [switchId, state_str](const String &var) {
      return SwitchAPIHandlers::genericMessageTemplateProcessor(var, "Switch " + switchId + " was turned " + state_str);
    });
  }

  return request->send_P(400, "application/json", genericResponse, [](const String &var) {
    return SwitchAPIHandlers::genericMessageTemplateProcessor(var, String("Bad formatted request, please read the doc, dumb."));
  });
}

void SwitchAPIServices::getActiveLightsService(AsyncWebServerRequest *request, LightsMap &lightSwitches)
{
  request->send_P(200, "application/json", availableSwitchs, [&lightSwitches](const String &var) {
    return SwitchAPIHandlers::availableSwitchsTemplateProcessor(var, lightSwitches);
  });
}

/*Defining API Handlers*/
void SwitchAPIHandlers::notFound(AsyncWebServerRequest *request)
{
  request->send_P(400, "application/json", genericResponse, [](const String &var) {
    return SwitchAPIHandlers::genericMessageTemplateProcessor(var, String("Not Found"));
  });
}

String SwitchAPIHandlers::moduleTemplateProcessor(const String &var, String &module_name)
{
  if (var == "MODULE_NAME_P")
    return String(module_name);
  if (var == "MODULE_IP_P")
    return WiFi.localIP().toString();
  return String();
}

String SwitchAPIHandlers::availableSwitchsTemplateProcessor(const String &var, LightsMap &lightSwitches)
{
  if (var == "SWITCH_1")
    return lightSwitches["SWITCH_1"].available ? "ON" : "OFF";
  if (var == "SWITCH_2")
    return lightSwitches["SWITCH_2"].available ? "ON" : "OFF";
  if (var == "SWITCH_3")
    return lightSwitches["SWITCH_3"].available ? "ON" : "OFF";
  return String();
}

String SwitchAPIHandlers::genericMessageTemplateProcessor(const String &var, String message)
{
  if (var == "MESSAGE_P")
    return message;
  return String();
}

String SwitchAPIHandlers::lightStateTemplateProcessor(const String &var, String lightId, uint8_t state)
{
  if (var == "LIGHT_ID_P")
    return lightId;

  if (var == "LIGHT_STATE_P")
    return String(state ? "ON" : "OFF");
  return String();
}