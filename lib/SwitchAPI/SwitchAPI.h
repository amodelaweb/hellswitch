#ifndef HELL_LIGHT_API_CONST_H
#define HELL_LIGHT_API_CONST_H

#include <SwitchCore.h>

//Core Switches
#define LIGHT_ID "lightId"
#define SWITCH_ID_P "switchId"
#define STATE_P "light_state"
#define STATE_SP "switch_state"

namespace SwitchAPIServices
{
    /*Defining API services*/
    void getModuleInformationService(AsyncWebServerRequest *request, String &module_name);
    void getLightStateService(AsyncWebServerRequest *request, LightsMap &lightTriggers);
    void setLightStateService(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total, LightsMap &lightTriggers);
    void setSwitchStateService(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total, LightsMap &lightTriggers, LightsMap &lightSwitches, NamingMap &switchMapping);
    void getActiveLightsService(AsyncWebServerRequest *request, LightsMap &lightSwitches);
    void toggleLightStateService(AsyncWebServerRequest *request, LightsMap &lightTriggers);

} // namespace SwitchAPIServices

namespace SwitchAPIHandlers
{
    /*Defining API Handlers*/
    void notFound(AsyncWebServerRequest *request);

    String moduleTemplateProcessor(const String &var, String &module_name);
    String availableSwitchsTemplateProcessor(const String &var, LightsMap &lightSwitches);
    String genericMessageTemplateProcessor(const String &var, String message);
    String lightStateTemplateProcessor(const String &var, String lightId, uint8_t state);

} // namespace SwitchAPIHandlers

//Core Switch
extern const char *lightStateTemplate;
extern const char *availableSwitchs;
extern const char *myDataTemplate;
extern const char *genericResponse;

#endif