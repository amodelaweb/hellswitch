#include "SwitchCore.h"

void SwitchCore::checkSwitchesState(LightsMap &lightTriggers, LightsMap &lightSwitches, NamingMap &switchMapping, AsyncMqttClient &mqttClient, master_data_t &switchData)
{
    uint8_t temporaLIGHT_PIN_state;
    for (unsigned int i = 0; i < lightSwitches.size(); i++)
    {
        temporaLIGHT_PIN_state = digitalRead(lightSwitches.valueAt(i).pin);
        if (temporaLIGHT_PIN_state != lightSwitches.valueAt(i).state)
        {
            char *state = !lightTriggers[switchMapping[lightSwitches.keyAt(i)]].state ? (char *)"ON" : (char *)"OFF";
            char *topic;
            if (!strcmp(switchMapping[lightSwitches.keyAt(i)], "LIGHT_1"))
                topic = (char *)switchData.topic_switch1.c_str();
            else if (!strcmp(switchMapping[lightSwitches.keyAt(i)], "LIGHT_2"))
                topic = (char *)switchData.topic_switch2.c_str();
            else if (!strcmp(switchMapping[lightSwitches.keyAt(i)], "LIGHT_3"))
                topic = (char *)switchData.topic_switch3.c_str();

            lightSwitches[lightSwitches.keyAt(i)].state = temporaLIGHT_PIN_state;
            SwitchCore::toggleLight(switchMapping[lightSwitches.keyAt(i)], lightTriggers);
            SwitchCore::publishToBroker(mqttClient, topic, state, 1, false);
            Serial.printf("\n Pin %s changed !!", lightSwitches.keyAt(i));
        }
    }
}

void SwitchCore::toggleLight(const char *lightId, LightsMap &lightTriggers)
{
    uint8_t newState = !lightTriggers[lightId].state;
    lightTriggers[lightId].state = newState;
    digitalWrite(lightTriggers[lightId].pin, newState);
}

uint8_t SwitchCore::getLightState(const char *lightId, LightsMap &lightTriggers)
{
    return lightTriggers[lightId].state;
}

void SwitchCore::setLightState(const char *lightId, uint8_t state, LightsMap &lightTriggers)
{
    lightTriggers[lightId].state = state;
    digitalWrite(lightTriggers[lightId].pin, state);
}

void SwitchCore::setSwitchState(const char *switchId, boolean state, LightsMap &lightTriggers, LightsMap &lightSwitches, NamingMap &switchMapping)
{
    lightTriggers[switchMapping[switchId]].available = state;
    lightSwitches[switchId].available = state;
}

void SwitchCore::publishToBroker(AsyncMqttClient &mqttClient, char *topic, char *message, uint8_t qos, bool retain)
{
    mqttClient.publish(topic, qos, retain, message);
    Serial.printf("\nPublishing in the topic %s the message %s at QoS %d\n", topic, message, qos);
}