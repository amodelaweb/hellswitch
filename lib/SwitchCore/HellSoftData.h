#ifndef HELL_SOFT_DATA_H
#define HELL_SOFT_DATA_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>
#include <DNSServer.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWiFiManager.h>
#include <asyncHTTPrequest.h>
#include <ArduinoJson.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>
#include "ArduinoMap.h"

typedef struct
{
    uint8_t pin;
    uint8_t state;
    boolean available;
} pin_t;

typedef struct {
    char module_name[25];
    char master_ip[16];
    char master_endpoint[25];
    char mqtt_user[50];
    char mqtt_password[50];
    uint16_t mqtt_port;
    char switch_1[4];
    char switch_2[4];
    char switch_3[4];
    String topic_switch1;
    String topic_switch2;
    String topic_switch3;
} master_data_t;

#endif