#ifndef HELLIGHT_CORE_CONST_H
#define HELLIGHT_CORE_CONST_H

#include "HellSoftData.h"

#define LIGHTSWITCH_PIN_NUMBER 3

typedef Map<const char *, const char *, LIGHTSWITCH_PIN_NUMBER> NamingMap;
typedef Map<const char *, pin_t, LIGHTSWITCH_PIN_NUMBER> LightsMap;

namespace SwitchCore
{
    void checkSwitchesState(LightsMap &lightTriggers, LightsMap &lightSwitches, NamingMap &switchMapping, AsyncMqttClient &mqttClient, master_data_t &switchData);
    void toggleLight(const char *lightId, LightsMap &lightTriggers);
    uint8_t getLightState(const char *lightId, LightsMap &lightTriggers);
    void setLightState(const char *lightId, uint8_t state, LightsMap &lightTriggers);
    void setSwitchState(const char *switchId, boolean state, LightsMap &lightTriggers, LightsMap &lightSwitches, NamingMap &switchMapping);
    void publishToBroker(AsyncMqttClient &mqttClient, char *topic, char *message, uint8_t qos, bool retain);
} // namespace SwitchCore

#endif