#ifndef HELL_LIGHT_ON_START_CONST_H
#define HELL_LIGHT_ON_START_CONST_H

#include <SwitchAPI.h>
#include <SwitchMqttClient.h>
#include <SwitchHTTPClient.h>

#define DEF_TIMEOUT 3000
#define DEF_TIMEOUT_SEC 600
#define DEF_SERIALIGHT_PIN_OUT 115200
#define SWITCH_ID "00001"
#define LIGHTSWITCH_PIN_NUMBER 3

//Light and Switchs Pins
#define LIGHT_PIN_1 12
#define LIGHT_PIN_2 13
#define LIGHT_PIN_3 14

#define SWITCH_PIN_1 16
#define SWITCH_PIN_2 4
#define SWITCH_PIN_3 5

//Master reporter
#define REPORT_TO_MASTER_ENDPOINT "/V1/Slaves/Create"

namespace SwitchStartUp
{
    bool autoConfigureWifi(AsyncWebServer &server, DNSServer &dns, master_data_t &switch_data);
    void initHttpServer(AsyncWebServer &server, LightsMap &lightTriggers, LightsMap &lightSwitches, NamingMap &switchMapping, master_data_t &switch_data);
    void initMapSwitches(NamingMap &switchMapping);
    void initLightPins(LightsMap &lightTriggers, master_data_t &switch_data);
    void initSwitchPins(LightsMap &lightSwitches, master_data_t &switch_data);
    void reportToMaster(asyncHTTPrequest &requestClient, master_data_t &switch_data);
    void initMQTTClient(AsyncMqttClient &mqttClient, Ticker &mqttReconnectTimer, LightsMap &lightTriggers, master_data_t &switch_data);
    void initHTTPClient(asyncHTTPrequest &requestClient, boolean debug);
} // namespace SwitchStartUp

namespace SwitchUtil
{
    bool stringComparator(const char *a, const char *b);
    boolean convertStringToBoolean(char *state);
} // namespace SwitchUtil

#endif