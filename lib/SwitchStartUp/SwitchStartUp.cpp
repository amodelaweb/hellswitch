#include "SwitchStartUp.h"

bool SwitchStartUp::autoConfigureWifi(AsyncWebServer &server, DNSServer &dns, master_data_t &switch_data)
{
    Serial.println("Configuring default WIFI AP");

    AsyncWiFiManager wifiManager(&server, &dns);
    char *default_ssid = new char[sizeof("Hell-Soft-ESP-") + sizeof(SWITCH_ID) + 1];
    char *default_password = new char[sizeof("working_gnu_") + sizeof(SWITCH_ID) + 1];

    AsyncWiFiManagerParameter custom_module_name("module_name", "Name Module", switch_data.module_name, 24);
    AsyncWiFiManagerParameter custom_master_ip("master_ip", "Master IP", switch_data.master_ip, 15);
    AsyncWiFiManagerParameter custom_master_endpoint("master_endpoint", "Master endpoint path", switch_data.master_endpoint, 25);
    AsyncWiFiManagerParameter custom_switch_1("switch_1", "Swith 1 state", switch_data.switch_1, 3);
    AsyncWiFiManagerParameter custom_switch_2("switch_2", "Swith 2 state", switch_data.switch_2, 3);
    AsyncWiFiManagerParameter custom_switch_3("switch_3", "Swith 3 state", switch_data.switch_3, 3);
    AsyncWiFiManagerParameter custom_mqtt_user("mqtt_user", "Mqtt User", switch_data.mqtt_user, 49);
    AsyncWiFiManagerParameter custom_mqtt_password("mqtt_password", "Mqtt Password", switch_data.mqtt_password, 49);

    wifiManager.addParameter(&custom_module_name);
    wifiManager.addParameter(&custom_master_ip);
    wifiManager.addParameter(&custom_master_endpoint);
    wifiManager.addParameter(&custom_switch_1);
    wifiManager.addParameter(&custom_switch_2);
    wifiManager.addParameter(&custom_switch_3);
    wifiManager.addParameter(&custom_mqtt_user);
    wifiManager.addParameter(&custom_mqtt_password);

    strcat(default_ssid, "Hell-Soft-ESP-");
    strcat(default_ssid, SWITCH_ID);
    strcat(default_password, "working_gnu_");
    strcat(default_password, SWITCH_ID);
    wifiManager.setTimeout(DEF_TIMEOUT_SEC);

    if (!wifiManager.autoConnect(default_ssid, default_password))
    {
        Serial.println("Failed to connect and hit timeout");
        delay(DEF_TIMEOUT);
        return false;
    }

    strcpy(switch_data.module_name, custom_module_name.getValue());
    strcpy(switch_data.master_ip, custom_master_ip.getValue());
    strcpy(switch_data.master_endpoint, custom_master_endpoint.getValue());
    strcpy(switch_data.switch_1, custom_switch_1.getValue());
    strcpy(switch_data.switch_2, custom_switch_2.getValue());
    strcpy(switch_data.switch_3, custom_switch_3.getValue());
    strcpy(switch_data.mqtt_user, custom_mqtt_user.getValue());
    strcpy(switch_data.mqtt_password, custom_mqtt_password.getValue());

    delete[] default_ssid;
    delete[] default_password;
    return true;
}

void SwitchStartUp::initHttpServer(AsyncWebServer &server, LightsMap &lightTriggers, LightsMap &lightSwitches, NamingMap &switchMapping, master_data_t &switch_data)
{
    Serial.println("Starting init Http Server");
    const char *lightsPath = "/lights"; //Get by HTTP_GET
    const char *lightPath = "/light";   // Get specific light, Put for update specific statis TODO VIEW ID
    const char *modulePath = "/module"; //Get module data by HTTP_GET
    const char *toggleLightPath = "/toggle";
    const char *switchPath = "/switch";

    server.on(lightsPath, HTTP_GET, [&lightSwitches](AsyncWebServerRequest *request) {
        return SwitchAPIServices::getActiveLightsService(request, lightSwitches);
    });
    server.on(lightPath, HTTP_GET, [&lightTriggers](AsyncWebServerRequest *request) {
        return SwitchAPIServices::getLightStateService(request, lightTriggers);
    });
    server.on(
        lightPath, HTTP_PUT, [](AsyncWebServerRequest *request) {}, NULL, [&lightTriggers](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) { return SwitchAPIServices::setLightStateService(request, data, len, index, total, lightTriggers); });
    server.on(modulePath, HTTP_GET, [&switch_data](AsyncWebServerRequest *request) {
        String module_name = String(switch_data.module_name);
        return SwitchAPIServices::getModuleInformationService(request, module_name);
    });
    server.on(toggleLightPath, HTTP_PUT, [&lightTriggers](AsyncWebServerRequest *request) {
        return SwitchAPIServices::toggleLightStateService(request, lightTriggers);
    });
    server.on(
        switchPath, HTTP_PUT, [](AsyncWebServerRequest *request) {}, NULL, [&lightTriggers, &lightSwitches, &switchMapping](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) { return SwitchAPIServices::setSwitchStateService(request, data, len, index, total, lightTriggers, lightSwitches, switchMapping); });

    server.onNotFound(SwitchAPIHandlers::notFound);

    server.begin();
    Serial.println("Ending init Http Server");
}

void SwitchStartUp::initMapSwitches(NamingMap &switchMapping)
{
    Serial.println("Starting init Map switches");
    switchMapping["SWITCH_1"] = "LIGHT_1";
    switchMapping["SWITCH_2"] = "LIGHT_2";
    switchMapping["SWITCH_3"] = "LIGHT_3";
    Serial.println("Ending init Map switches");
}

void SwitchStartUp::initLightPins(LightsMap &lightTriggers, master_data_t &switch_data)
{
    Serial.println("Init Light triggers started!");
    lightTriggers["LIGHT_1"].pin = LIGHT_PIN_1;
    lightTriggers["LIGHT_1"].state = LOW;
    lightTriggers["LIGHT_1"].available = SwitchUtil::convertStringToBoolean(switch_data.switch_1);

    lightTriggers["LIGHT_2"].pin = LIGHT_PIN_2;
    lightTriggers["LIGHT_2"].state = LOW;
    lightTriggers["LIGHT_2"].available = SwitchUtil::convertStringToBoolean(switch_data.switch_2);

    lightTriggers["LIGHT_3"].pin = LIGHT_PIN_3;
    lightTriggers["LIGHT_3"].state = LOW;
    lightTriggers["LIGHT_3"].available = SwitchUtil::convertStringToBoolean(switch_data.switch_3);

    for (unsigned int i = 0; i < lightTriggers.size(); i++)
    {
        pinMode(lightTriggers.valueAt(i).pin, OUTPUT);
        digitalWrite(lightTriggers.valueAt(i).pin, LOW);
    }

    Serial.println("Init Light triggers finished!");
}

void SwitchStartUp::initSwitchPins(LightsMap &lightSwitches, master_data_t &switch_data)
{
    Serial.println("Init Switch pins started!");
    lightSwitches["SWITCH_1"].pin = SWITCH_PIN_1;
    lightSwitches["SWITCH_1"].available = SwitchUtil::convertStringToBoolean(switch_data.switch_1);

    lightSwitches["SWITCH_2"].pin = SWITCH_PIN_2;
    lightSwitches["SWITCH_2"].available = SwitchUtil::convertStringToBoolean(switch_data.switch_2);

    lightSwitches["SWITCH_3"].pin = SWITCH_PIN_3;
    lightSwitches["SWITCH_3"].available = SwitchUtil::convertStringToBoolean(switch_data.switch_3);

    for (unsigned int i = 0; i < lightSwitches.size(); i++)
    {
        pinMode(lightSwitches.valueAt(i).pin, INPUT);
    }
    IPAddress();
    lightSwitches["SWITCH_1"].state = digitalRead(SWITCH_PIN_1);
    lightSwitches["SWITCH_2"].state = digitalRead(SWITCH_PIN_2);
    lightSwitches["SWITCH_3"].state = digitalRead(SWITCH_PIN_3);
    Serial.println("Init Switch pins finished!");
}

void SwitchStartUp::reportToMaster(asyncHTTPrequest &requestClient, master_data_t &switch_data)
{
    Serial.println("Starting report to master");
    std::string body = std::string("{ \"switch_1\" : \"") + switch_data.switch_1 + "\", \"switch_2\" : \"" + switch_data.switch_2 + "\", \"switch_3\" : \"" + switch_data.switch_3 + "\"}" ;
    std::string url = std::string("http://") + switch_data.master_ip + REPORT_TO_MASTER_ENDPOINT;
    std::string method = std::string("POST");
    
    HTTPClient::sendRequest(requestClient, url.c_str(), method.c_str(), body.c_str());
    Serial.println("Ending report to master");
}

void SwitchStartUp::initMQTTClient(AsyncMqttClient &mqttClient, Ticker &mqttReconnectTimer, LightsMap &lightTriggers, master_data_t &switch_data)
{
    Serial.println("Init MQTT Client");
    mqttClient.onConnect([&mqttClient, &switch_data](bool sessionPresent) {
        return MqttHandlers::onMqttConnect(mqttClient, sessionPresent, switch_data);
    });
    mqttClient.onDisconnect([&mqttClient, &mqttReconnectTimer](AsyncMqttClientDisconnectReason reason) {
        return MqttHandlers::onMqttDisconnect(mqttClient, mqttReconnectTimer, reason);
    });
    mqttClient.onSubscribe(MqttHandlers::onMqttSubscribe);
    mqttClient.onUnsubscribe(MqttHandlers::onMqttUnsubscribe);
    mqttClient.onMessage([&switch_data, &lightTriggers](char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
        return MqttHandlers::onMqttMessage(lightTriggers, switch_data, topic, payload, properties, len, index, total);
    });
    mqttClient.onPublish(MqttHandlers::onMqttPublish);
    mqttClient.setServer((char *)switch_data.master_ip, switch_data.mqtt_port);
    mqttClient.setCredentials((char *)switch_data.mqtt_user, (char *)switch_data.mqtt_password);
    mqttClient.setCleanSession(true);
    MqttHandlers::connectToMqtt(mqttClient);
    Serial.println("Ending Init MQTT Client");
}

void SwitchStartUp::initHTTPClient(asyncHTTPrequest &requestClient, boolean debug)
{
    Serial.println("Start Init HTPP Client");
    requestClient.setDebug(debug);
    requestClient.onReadyStateChange(HTTPHandlers::requestCB);
    Serial.println("Ending init HTPP Client");
}

/* Utilities */
bool SwitchUtil::stringComparator(const char *a, const char *b)
{
    return !strcmp(a, b) ? true : false;
}

boolean SwitchUtil::convertStringToBoolean(char *state)
{
    return !strcmp("ON", state);
}
