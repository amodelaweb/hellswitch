#include "SwitchMqttClient.h"

/* Mqtt Handlers */
//Taked from https://github.com/marvinroger/async-mqtt-client/blob/master/examples/FullyFeatured-ESP8266/FullyFeatured-ESP8266.ino
void MqttHandlers::connectToMqtt(AsyncMqttClient &mqttClient)
{
    Serial.println("Connecting to MQTT...");
    mqttClient.connect();
}

void MqttHandlers::onMqttConnect(AsyncMqttClient &mqttClient, bool sessionPresent, master_data_t &switchData)
{
    //To take care /qos 0 es low security and 2 the highest level
    Serial.println("Connected to MQTT.");
    Serial.print("Session present: ");
    Serial.println(sessionPresent);

    //Subscribe to the command handler
    mqttClient.subscribe(String(switchData.topic_switch1 + "/command").c_str(), 1);
    mqttClient.subscribe(String(switchData.topic_switch2 + "/command").c_str(), 1);
    mqttClient.subscribe(String(switchData.topic_switch3 + "/command").c_str(), 1);
}

void MqttHandlers::onMqttDisconnect(AsyncMqttClient &mqttClient, Ticker &mqttReconnectTimer, AsyncMqttClientDisconnectReason reason)
{
    Serial.println("Disconnected from MQTT.");
    Serial.println(String((int8_t)reason).c_str());
    mqttReconnectTimer.once(2, [&mqttClient]() {
        return MqttHandlers::connectToMqtt(mqttClient);
    });
}

void MqttHandlers::onMqttSubscribe(uint16_t packetId, uint8_t qos)
{
    Serial.println("Subscribe acknowledged.");
    Serial.print("  packetId: ");
    Serial.println(packetId);
    Serial.print("  qos: ");
    Serial.println(qos);
}

void MqttHandlers::onMqttUnsubscribe(uint16_t packetId)
{
    Serial.println("Unsubscribe acknowledged.");
    Serial.print("  packetId: ");
    Serial.println(packetId);
}

void MqttHandlers::onMqttMessage(LightsMap &lightTriggers, master_data_t &switchData, char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{ 
    char payload_reduced[len+1];
    strncpy(payload_reduced, payload, len);
    payload_reduced[len] = '\0';

    uint8_t state = 1 ? !strcmp("ON", payload_reduced) : 0;
    Serial.printf("\nRECIBI MENSAJE = %s", payload_reduced);
    Serial.printf("\nRECIBI TOPIC = %s", topic);
    if (!strcmp(topic, String(switchData.topic_switch1 + "/command").c_str()))
        SwitchCore::setLightState("LIGHT_1", state, lightTriggers);
    else if (!strcmp(topic, String(switchData.topic_switch2 + "/command").c_str()))
        SwitchCore::setLightState("LIGHT_2", state, lightTriggers);
    else if (!strcmp(topic, String(switchData.topic_switch3 + "/command").c_str()))
        SwitchCore::setLightState("LIGHT_3", state, lightTriggers);
}

void MqttHandlers::onMqttPublish(uint16_t packetId)
{
    Serial.println("Publish acknowledged.");
    Serial.print("  packetId: ");
    Serial.println(packetId);
}