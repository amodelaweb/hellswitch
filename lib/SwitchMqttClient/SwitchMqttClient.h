#ifndef HELLIGHT_MQTT_H
#define HELLIGHT_MQTT_H

#include <SwitchCore.h>

namespace MqttHandlers
{
    void connectToMqtt(AsyncMqttClient &mqttClient);
    void onMqttConnect(AsyncMqttClient &mqttClient, bool sessionPresent, master_data_t &switchData);
    void onMqttDisconnect(AsyncMqttClient &mqttClient, Ticker &mqttReconnectTimer, AsyncMqttClientDisconnectReason reason);
    void onMqttSubscribe(uint16_t packetId, uint8_t qos);
    void onMqttUnsubscribe(uint16_t packetId);
    void onMqttMessage(LightsMap &lightTriggers, master_data_t &switchData, char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total);
    void onMqttPublish(uint16_t packetId);
} // namespace MqttHandlers

//Core MQTT Client
extern char mqttBroker[60];

#endif