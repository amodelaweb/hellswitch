#include <SwitchStartUp.h>

//Switch pins and states
Map<const char *, pin_t, LIGHTSWITCH_PIN_NUMBER> lightTriggers(SwitchUtil::stringComparator);
Map<const char *, pin_t, LIGHTSWITCH_PIN_NUMBER> lightSwitches(SwitchUtil::stringComparator);
Map<const char *, const char *, LIGHTSWITCH_PIN_NUMBER> switchMapping(SwitchUtil::stringComparator);

//API Templates
const char *lightStateTemplate = R"({"ApiConstants::LIGHT_ID":"%LIGHT_ID_P%", "light_state":"%LIGHT_STATE_P%"})";
const char *availableSwitchs = R"({"switchs": ["switch_1":"%SWITCH_1%", "switch_2":"%SWITCH_2%", "switch_3":"%SWITCH_3%"]})";
const char *myDataTemplate = R"({"module_name":"%MODULE_NAME_P%", "module_ip":"%MODULE_IP_P%"})";
const char *genericResponse = R"({"message" : "%MESSAGE_P%"})";

//Core data
master_data_t master_data = {
    "MODULE_", 
    "10.69.0.26",
    "/api.json",
    "torvalds",
    "workinggnu",
    1883,
    "ON",
    "ON",
    "ON",
    "/hell/light_1",
    "/hell/light_2",
    "/hell/light_3"
};

Ticker mqttReconnectTimer;
AsyncMqttClient mqttClient;
asyncHTTPrequest requestClient;
AsyncWebServer server(80);
DNSServer dns;

void setup()
{
  Serial.begin(DEF_SERIALIGHT_PIN_OUT);
  strcat(master_data.module_name, SWITCH_ID);
  bool networkAccess = SwitchStartUp::autoConfigureWifi(server, dns, master_data);
  SwitchStartUp::initSwitchPins(lightSwitches, master_data);
  SwitchStartUp::initLightPins(lightTriggers, master_data);
  SwitchStartUp::initMapSwitches(switchMapping);
  if (networkAccess)
  {
    SwitchStartUp::initHttpServer(server, lightTriggers, lightSwitches, switchMapping, master_data);
    delay(DEF_TIMEOUT);                                                                        //Delay after init HTTP Server
    SwitchStartUp::initMQTTClient(mqttClient, mqttReconnectTimer, lightTriggers, master_data); //PENDING TPDP
    delay(DEF_TIMEOUT);                                                                        //Delay after init HTTP Server
    SwitchStartUp::initHTTPClient(requestClient, true);
    delay(DEF_TIMEOUT);                                                                        //Delay after init HTTP Server
    SwitchStartUp::reportToMaster(requestClient, master_data);
    delay(DEF_TIMEOUT); //Delay after HTTP Client operation
  }
  Serial.println("Setup done --");
  WiFi.begin();
}

void loop()
{
  SwitchCore::checkSwitchesState(lightTriggers, lightSwitches, switchMapping, mqttClient, master_data); //Check if manual switches changed
  delay(100);
}
