# ESP8266 Smart Switch

Smart switch made using the ESP8266.

#### Basic features
                
+ Toggle light using the physical switch
+ Toggle light using the REST API inside the ESP8266
+ Toggle light using the MQTT client
+ Update lights when state changed

#### Usage and development
The project was developed using Platform.io, so you can compile it from the IDE.

#### PCB Design
The PCB Used for this project was designed using the schema above

> SOON

#### Tasks to do

- [x] MQTT Client
- [x] REST Server
- [x] Manual Switch updated
- [ ] Auto discovery for Domoticz

By: Santiago Ch.

![](https://gitlab.com/amodelaweb/hellswitch/-/raw/master/doc/images/logo_v1.png)




